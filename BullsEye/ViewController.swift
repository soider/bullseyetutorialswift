//
//  ViewController.swift
//  BullsEye
//
//  Created by Михаил Сахнов on 21.08.15.
//  Copyright (c) 2015 Михаил Сахнов. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var currentValue: Int = 50
    var targetValue: Int = 0
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetLabel: UILabel!

    @IBAction func showAlert() {
        let message = "Slider value is \(currentValue) and target is \(targetValue)"
        let title = "Hello, world"
        let alert = UIAlertController(title: title,
            message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
        initGame()
    }
    
    @IBAction func sliderMoved(slider: UISlider) {
            currentValue = lroundf(slider.value)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initGame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startNewRoundListener() {
        initGame()
    }
    
    func initGame() {
        startNewRound()
        updateLabels()
    }
    
    func startNewRound() {
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        slider.value = Float(currentValue)
    }
    
    func updateLabels() {
        targetLabel.text = String(targetValue)
    }


}

